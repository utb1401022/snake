﻿namespace Snake
{
    class Program
    {
        static void Main(string[] args)
        {
            var game = new Game(width: 32, height: 16, score: 5);
            game.Run();
        }            
    }
}