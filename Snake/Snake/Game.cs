﻿
using System.Drawing;

namespace Snake
{
    public class Game(int width, int height, int score)
    {
        private readonly Random generator = new Random();
        private readonly int gameSpeed = 300;

        public void Run()
        {
            ResizeConsole();

            var snake = new Snake(width / 2, height / 2, Direction.Right);
            var berry = GetRandomPixel((0, width), (0, height));

            while (true)
            {
                if (snake.IsDead(width, height))
                {
                    break;
                }

                if (berry.IsSameAs(snake.Head))
                {
                    score++;
                    berry = GetRandomPixel((1, width - 2), (1, height - 2));
                }

                DrawScene(snake, berry);
                ProgressGameOneStep(snake);
            }

            Console.SetCursorPosition(width / 5, height / 2);
            Console.WriteLine("Game over, Score: " + score);
            Console.SetCursorPosition(width / 5, height / 2 + 1);
        }

        private void ProgressGameOneStep(Snake snake)
        {
            var gameTick = DateTime.Now;
            while (true)
            {
                if (DateTime.Now.Subtract(gameTick).TotalMilliseconds > gameSpeed)
                {
                    break;
                }

                if (Console.KeyAvailable)
                {
                    snake.Direction = CalculateNewDirection(Console.ReadKey(true).Key, snake.Direction);
                }
            }

            snake.Tail.Add(new Pixel() 
            { 
                X = snake.Head.X, 
                Y = snake.Head.Y 
            });

            switch (snake.Direction)
            {
                case Direction.Up:
                    snake.Head.Y--;
                    break;
                case Direction.Down:
                    snake.Head.Y++;
                    break;
                case Direction.Left:
                    snake.Head.X--;
                    break;
                case Direction.Right:
                    snake.Head.X++;
                    break;
            }
            
            if (snake.Tail.Count() > score)
            {
                snake.Tail.RemoveAt(0);
            }
        }

        private void DrawScene(Snake snake, Pixel berry)
        {
            Console.Clear();

            // arena
            for (int i = 0; i < width; i++)
            {
                Console.SetCursorPosition(i, 0);
                Console.Write("■");
                Console.SetCursorPosition(i, height - 1);
                Console.Write("■");

            }
            for (int i = 0; i < height; i++)
            {
                Console.SetCursorPosition(0, i);
                Console.Write("■");
                Console.SetCursorPosition(width - 1, i);
                Console.Write("■");
            }

            // snake
            Console.ForegroundColor = ConsoleColor.Green;
            foreach (var pixel in snake.Tail)
            {
                Console.SetCursorPosition(pixel.X, pixel.Y);
                Console.Write("■");
            }

            Console.ForegroundColor = ConsoleColor.Red;
            Console.SetCursorPosition(snake.Head.X, snake.Head.Y);
            Console.Write("■");

            // berry
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.SetCursorPosition(berry.X, berry.Y);
            Console.Write("■");

        }

        private Direction CalculateNewDirection(ConsoleKey key, Direction direction)
        {
            return (key, direction) switch
            {
                (ConsoleKey.UpArrow, Direction.Left) => Direction.Up,
                (ConsoleKey.UpArrow, Direction.Right) => Direction.Up,
                (ConsoleKey.DownArrow, Direction.Left) => Direction.Down,
                (ConsoleKey.DownArrow, Direction.Right) => Direction.Down,
                (ConsoleKey.LeftArrow, Direction.Up) => Direction.Left,
                (ConsoleKey.LeftArrow, Direction.Down) => Direction.Left,
                (ConsoleKey.RightArrow, Direction.Up) => Direction.Right,
                (ConsoleKey.RightArrow, Direction.Down) => Direction.Right,
                _ => direction
            };
        }

        private void ResizeConsole()
        {
            Console.WindowHeight = height;
            Console.WindowWidth = width;
        }

        private Pixel GetRandomPixel((int, int) xRange, (int, int) yRange)
        {
            return new Pixel()
            { 
                X = generator.Next(xRange.Item1, xRange.Item2), 
                Y = generator.Next(yRange.Item1, yRange.Item2)
            };
        }
    }

    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }
}