﻿namespace Snake
{
    public record Pixel
    {
        public int X { get; set; }
        public int Y { get; set; }

        public bool IsSameAs(Pixel pixel)
        {
            return X == pixel.X && Y == pixel.Y;
        }
    }
}
