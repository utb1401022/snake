﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake
{
    public class Snake
    {
        public Pixel Head { get; set; }

        public List<Pixel> Tail { get; set; }

        public Direction Direction { get; set; }

        public Snake(int x, int y, Direction direction)
        {
            Head = new Pixel
            {
                X = x,
                Y = y
            };
            Direction = direction;
            Tail = new List<Pixel>();                
        }

        public bool IsDead(int width, int height)
        {
            return Head.X < 1 || Head.X > (width - 2) || Head.Y < 1 || Head.Y > (height -2) || Tail.Any(pixel => pixel.IsSameAs(Head));
        }
    }
}
